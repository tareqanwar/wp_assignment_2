<?php get_header(); ?>

<div id="primary" class="col-xs-12 col-sm-9">

<h1 class="archive_title"><?=wp_title('', true,'');?></h1>

<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<!-- post title -->
		<h2>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
		</h2>
		<!-- /post title -->

		<!-- post details -->
		<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
		
		<?php the_excerpt(); // Build your custom callback length in functions.php ?>

	</article>
	<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'afterschool' ); ?></h2>
	</article>
	<!-- /article -->

	<?php endif; ?>

				
</div>
<!-- /primary -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
