<?php
/*------------------------------------*\
	Functions
\*------------------------------------*/

// Westland menus
function register_westland_menus() {
register_nav_menus(
    array(
    'main-menu' => __( 'Main Menu' ),
    'util-menu' => __( 'Util Menu' ),
    'footer-menu' => __( 'Footer Menu' ),
    'second-footer-menu' => __( 'Second Footer Menu' )
    )
);
}
add_action( 'init', 'register_westland_menus' );

// Load styles & scripts
function register_westland_scripts() {
    wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', array(), 20141119 );
    wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/style.css', array(), 20141119 );
    // all scripts
    wp_enqueue_script( 'bootstrap', 'https//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', array('jquery'), '20120206', true );
    wp_enqueue_script( 'theme-script', get_template_directory_uri() . '/js/script.js', array('jquery'), '20120206', true );
}
add_action( 'wp_enqueue_scripts', 'register_westland_scripts' );

//enable featured image support
add_theme_support( 'post-thumbnails' ); 

//add body class
function westland_body_class( $classes ) {
 
    if ( is_page_template( 'template-home.php' ) ) {
        $classes[] = 'home';
    }
    else {
        $classes[] = 'default';
    }
     
    return $classes;
     
}
add_filter( 'body_class','westland_body_class' );

//Use testimonials category template for sub categories of testimonials category
function testimonialsTemplateSelect() {
    if (is_category() && !is_feed()) {
        if (is_category(get_cat_id('testimonials')) || cat_is_ancestor_of(get_cat_id('testimonials'), get_query_var('cat'))) {
            load_template(TEMPLATEPATH . '/category-testimonials.php');
            exit;
        }
    }
}

add_action('template_redirect', 'testimonialsTemplateSelect');