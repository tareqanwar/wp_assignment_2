<div id="secondary" class="col-xs-12 col-sm-3">
<h3>Section menu</h3>
<ul>
<?php
$cats = get_the_category();
$parent = $cats[0]->category_parent;
$categories = get_categories(
    array(
        'child_of' => $parent,
        'orderby' => 'id',
        'order' => 'ASC',
        'exclude' => $catid,
        'hide_empty' => '0'
    )
); 

foreach ($categories as $category) {
    $active = ( $category->cat_ID == $cats[0]->cat_ID ) ? 'active' : '';
    echo '<li><a href="'.get_category_link($category->cat_ID).'" class="'. $active .'">.. '.$category->cat_name.'</a></li>';
}
?>
</ul>

</div><!-- /secondary -->