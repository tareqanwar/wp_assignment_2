<?php 
/* Template Name: Home Page Template */ 
get_header(); 
?>

<div id="featured">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<?php the_content() ?>
	<?php endwhile; ?> <?php endif; ?>
</div>

<div id="callouts" class="col-xs-12">
	<?php 
	global $post;

	$args = array( 'numberposts' => 3, 'category_name' => 'Callouts' );

	$posts = get_posts( $args );

	foreach( $posts as $post ): setup_postdata($post); 
		// check if the post has a Post Thumbnail assigned to it.
		?>
		<div class="col-xs-4">
			<?php
			if ( has_post_thumbnail() ) {
			?>
				<a href="<?=the_permalink();?>"><?=the_post_thumbnail();?></a>
			<?php
			} 
			?>
			<h2><?=the_title()?></h2>
			<?=the_excerpt()?>
			<a href="<?=the_permalink();?>" class="readmore">Read more ></a>
		</div>
	<?php endforeach; ?>
</div><!-- /featured -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>
