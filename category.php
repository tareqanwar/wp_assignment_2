<?php
get_header(); 
get_sidebar('category');
?>

	<div id="primary" class="col-xs-12 col-sm-9">

        <?php 
            $cat = get_the_category();
            $parentCatName = get_cat_name($cat[0]->parent);

            echo "<h1 class='cat_title'>" . $parentCatName ."</h1>";
            echo "<h2 class='cat_title'>" . single_cat_title('', false) ."</h2>";
        ?>

        <?php if ( have_posts() ) : ?>

            <?php
                // Start the Loop.
            while ( have_posts() ) :
                the_post(); ?>
                <!-- article -->
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="row thumbnail col-sm-12">
                        <div id="cat_thumb" class="col-xs-12 col-sm-3">
                            <!-- post thumbnail -->
                            <?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <?php the_post_thumbnail(); // Fullsize image for the single post ?>
                                </a>
                            <?php endif; ?>
                            <!-- /post thumbnail -->
                        </div>
                        <div id="category_post" class="col-xs-12 col-sm-9">

                            <!-- post title -->
                            <h2>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <!-- /post title -->

                            <!-- post details -->
                            
                            <?php the_excerpt(); // Build your custom callback length in functions.php ?>

                            <a href="<?php the_permalink(); ?>" class="learnmore">Learn more  >></a>
                        </div>
                    </div>

                </article>
                <!-- /article -->

                <?php endwhile; ?>

                <?php else: ?>

                <!-- article -->
                <article>
                    <h2><?php _e( 'Sorry, nothing to display.', 'a2' ); ?></h2>
                </article>
                <!-- /article -->

                <?php endif; ?>

                            
            </div>
            <!-- /primary -->

<?php
get_footer();