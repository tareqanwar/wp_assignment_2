<?php get_header(); ?>

	<div id="primary" class="col-xs-12 col-sm-12">

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<!-- post thumbnail -->
				<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<?php the_post_thumbnail(); // Fullsize image for the single post ?>
					</a>
				<?php endif; ?>
				<!-- /post thumbnail -->

				<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
		
				<p><span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span></p>
				
				<?php the_content(); ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

			<?php comments_template(); ?>

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'afterschool' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>
				
		</div>
		<!-- /primary -->

<?php get_footer(); ?>
