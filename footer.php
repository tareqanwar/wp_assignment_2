			</div>
			<!-- /content -->

			<!-- footer -->
			<footer class="main col-xs-12">
				<div class="row">
						<div class="col-xs-0 col-sm-2">
							<img src="<?php echo get_template_directory_uri(); ?>/images/dog-icon.png" alt="Dog" />
						</div>
						<div class="col-xs-12  col-sm-8">
							<nav id="footer">
							
								<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
								<?php wp_nav_menu( array( 'theme_location' => 'second-footer-menu' ) ); ?>
							
							</nav>

							<p class="copyright">Contents copyright &copy; 2010 - <?php echo date('Y'); ?> <?php echo  bloginfo('name');?> </p>
						</div>
						<div class="col-xs-0  col-sm-2">
							<img src="<?php echo get_template_directory_uri(); ?>/images/wordmark.png" id="wordmark" alt="Westland"  />
						</div>
				</div>
			</footer>
		
		</div><!-- /container -->

		<?php wp_footer(); ?>

	</body>
</html>
