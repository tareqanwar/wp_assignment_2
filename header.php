<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>

	</head>
	<body <?php body_class(); ?>>
		<div class="container">
				
			<nav id="util">
				<?php wp_nav_menu( array( 'theme_location' => 'util-menu' ) ); ?>
			</nav>
			
			<header class="main col-xs-12">

				<a href="<?php echo home_url(); ?>"><span class="site_title"></span></a>
				
				<a class="menu_toggle" href="#">Menu</a>

			</header>



			<nav id="main" class="col-xs-12" role="navigation">

				<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>

			</nav>
		
			<div id="content" class="col-xs-12">
